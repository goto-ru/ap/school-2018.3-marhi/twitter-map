from gevent.wsgi import WSGIServer
from webapp import app

http_server = WSGIServer(('', 30001), app)
http_server.serve_forever()
